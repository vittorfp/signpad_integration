var id = 0;
var hash;
var concludeMethod;
var task;

loadParams();
window.addEventListener("message", receiveMessage, false);
console.log("hash: " + hash);

function loadTask() {
  sendMessage("getUserTask", null);
}

function getExecutorCard() {
  var params = {};
  params.objectsReferences = [{'_id': task._executor._id, '_classId': task._executor._class._id}];

  sendMessage("getCards", params);
}

function loadExecutor() {
  var params = { 
    reference: JSON.parse(document.getElementById("userTask").value)._executor
  };
  sendMessage("get", params);
}

function updateDraft(data) {
  const keys  = Object.keys(data);
  keys.forEach(key => {
    task[key] = data[key];
  });
  var params = { 
    userTaskDraft: task
  };
  sendMessage("_updateDraft", params);
}

function conclude() {
  var params = {};
  if (concludeMethod) {
    params.concludeMethod = concludeMethod;
  }

  if (task) {    
    params.userTaskDraft = task;
  }
  sendMessage("conclude", params);
}

function getCardsForCombo() {
  var params = {};
  params.fieldIdentifier = document.getElementById("fieldIdentifier").value;
  params.from = document.getElementById("from").value;
  params.query = document.getElementById("query").value;

  sendMessage("getCardsForCombo", params);
}

function getMetadata() {
  sendMessage("_getMetadata", null);
}

function listConcludeMethods() {
  sendMessage("listConcludeMethods", null);
}

function deleteProcess() {
  sendMessage("deleteProcess", null);
}

function returnTask() {
  sendMessage("returnTask", null);
}

function sendMessage(type, params) {
  var message = {
      id: getId(),
      source: "CUSTOM_TASK_VIEW",
      type: type,
      hash: hash,
      params: params
  }
  console.log(message);
  window.parent.postMessage(JSON.stringify(message), '*');
}

function receiveMessage(event) {
  // alert(event.data);
  var message = JSON.parse(event.data);
  console.log(message);
  if (message.type == "getUserTask") {
    // document.getElementById("userTask").value = JSON.stringify(message.params.userTaskDraft);
    task = message.params.userTaskDraft;
  }
  if (message.type == "get") {
    task._executor = message.params.object;
    document.getElementById("userTask").value = JSON.stringify(task);
  }
  if (message.type == "updateDraft") {
    document.getElementById("userTask").value = JSON.stringify(message.params.userTaskDraft);
  }
  if (message.type == "listConcludeMethods") {
    concludeMethod = message.params.concludeMethods[0];
  }
  if (message.type == "getCards") {
    document.getElementById("executorCard").value = JSON.stringify(message.params.cardResult.cards[0]);
  }
  if (message.type == "conclude") {
    // alert("Atividade concluida");
  }
}

function loadParams() {
  hash = getURLParameter("hash");
}

function getId() {
  return id++;
}

function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

loadTask();
listConcludeMethods();