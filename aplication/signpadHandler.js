var canvas = null;
var ctx = null;
var pad = null;

function timedDetect() 
{
  if (wgssSignatureSDK.running) 
  {
    document.getElementById("statusText").innerHTML = "Signature SDK Service detected";
  } 
  else 
  {
    document.getElementById("statusText").innerHTML = "Signature SDK Service not detected";
  }
}
var timeout = setTimeout(timedDetect, 1500);

function onDetectRunning()
{
  document.getElementById("statusText").innerHTML = "Signature SDK Service detected";
  clearTimeout(timeout);
}
// pass the starting service port  number as configured in the registry
var wgssSignatureSDK = new WacomGSS_SignatureSDK(onDetectRunning, 8000)

function restartSession(onRestartSession)
{
  document.getElementById("statusText").innerHTML += "<br>Restarting the session";
  wgssSignatureSDK = new WacomGSS_SignatureSDK(onRestartSession, 8000)
}

function wizPin()
{
  var wizCtl;
  var sigCtl;
  var pad;
  var inputObj;
  
  function onDisplay(wizCtlV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Display";
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Display error";
    }
  }
  
  function onAddInputEcho(wizCtlV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl AddInputEcho";
      wizCtlV.Display(onDisplay);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl AddInputEcho error";
    }
  }
  
  function onPutFont2(wizCtlV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PutFont2";
      pad.x.Set("center");
	  
	  // Now set up the y axis position depending on which model pad it is
	  if (pad.Width == 396 && pad.Height == 100)
	  {
	     // This is an STU 300
         pad.y.Set(y = pad.Height / 2);
	  }
	  else
	  if (pad.Width == 320 && pad.Height == 200)
	  {
	     // This is a 430
        pad.y.Set(y = pad.Height / 2);
	  }
	  else
	  {
         pad.y.Set(y = pad.yText + 4*pad.yLSText);
	  }
	  
      var vIE = new wgssSignatureSDK.Variant();
      var option = new wgssSignatureSDK.Variant();
      option.Set(8);
      wizCtlV.AddObject(wgssSignatureSDK.ObjectType.ObjectInputEcho, "", pad.x, 
                        pad.y, vIE, option, onAddInputEcho);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PutFont2 error";
    }
  }
  
  function onAddInputObj(wizCtlV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl AddInputObj";
	  
	  // Now set up the pin size depending on which model pad it is
	  if (pad.Width == 396 && pad.Height == 100)
	  {
	     // This is an STU 300
         pad.PinSize = 10;
	  }
	  else
	  if (pad.Width == 320 && pad.Height == 200)
	  {
	     // This is a 430
        pad.PinSize = 12;
	  }
	  // Otherwise stick to default of 20
	  
      var font = new wgssSignatureSDK.Font(pad.Font, pad.PinSize);
      var varFont = new wgssSignatureSDK.Variant();
      varFont.Set(font);
      wizCtlV.PutFont(varFont, onPutFont2);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl AddInputObj error";
    }
  }
  
  function onPutMaxLengthInputObj(inputObjV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj PutMaxLength";
      pad.x.Set(0);
      pad.y.Set(0);
      var vIO = new wgssSignatureSDK.Variant();
      vIO.type = wgssSignatureSDK.VariantType.VARIANT_INPUTOBJ;
      vIO.handle = inputObjV.handle;
      var option = new wgssSignatureSDK.Variant();
      wizCtl.AddObject(wgssSignatureSDK.ObjectType.ObjectInput, "Input", pad.x, 
                        pad.y, vIO, option, onAddInputObj);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj PutMaxLength error";
    }
  }
  
  function onPutMinLengthInputObj(inputObjV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj PutMinLength";
      inputObjV.PutMaxLength(4, onPutMaxLengthInputObj);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj PutMinLength error";
    }
  }
  
  function onGetPropertyInputObj(inputObjV, property, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj GetProperty";
      inputObjV.PutMinLength(1, onPutMinLengthInputObj);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj GetProperty error";
    }
  }
  
  function onClearInputObj(inputObjV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj Clear";
      inputObjV.GetProperty("Component_FileVersion", onGetPropertyInputObj);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj Clear error";
    }
  }
  
  function onConstructorInputObj(inputObjV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj Constructor";
      inputObjV.Clear(onClearInputObj);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>InputObj Constructor error";
    }
  }  
  
  function onAddObjectOne(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>Add One Prompt";
      inputObj = new wgssSignatureSDK.InputObj(onConstructorInputObj);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Add One failed";
    }
  }
  
  function onAddObjectText(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>Add Text Prompt";
      
	  // The STU 500 values will work OK with the 520 and 530, but not the 300 and 430
	  if (pad.Width == 396 && pad.Height == 100)
	  {
	     // This is an STU 300
		 pad.x.num = pad.Width/6;
         pad.y.num = pad.Height/2;
	  }
	  else
	  if (pad.Width == 320 && pad.Height == 200)
	  {
	     // This is a 430
		 pad.x.num = pad.Width/2 - pad.KeyWidth/2 - 2*pad.KeyWidth;
         pad.y.num = pad.Height / 2;
	  }
	  else
	  { 
         pad.x.num = pad.Width/2 - pad.KeyWidth/2 - 2*pad.KeyWidth;
         pad.y.num = pad.yText + 7*pad.yLSText;
	  }
	  
      var txtOne = new wgssSignatureSDK.Variant();
      txtOne.Set("1");
      var option = new wgssSignatureSDK.Variant();
      option.Set(pad.KeyWidth);
      wizCtlG.AddObject(wgssSignatureSDK.ObjectType.ObjectButton, "1", pad.x, pad.y, txtOne, 
                        option, onAddObjectOne);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Text Prompt Add failed";
    }
  }
  
  function onGetPadWidth(wizCtlG, width, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl GetPadWidth";
      pad.Width = width;
  	  //document.getElementById("statusText").innerHTML += "<br>WizCtl GetPadWidth: " + pad.Width;

	    // Font sizes depend on the pad height and width
	    document.getElementById("statusText").innerHTML += "<br>Pad height and width: " + pad.Height + " " + pad.Width;
	  
      // Set up the font size depending on which pad is in use	 
      if (pad.Width == 396 && pad.Height == 100)
      {
         pad.TextSize = 8;
      }
      else
      if (pad.Width == 320 && pad.Height == 200)
      {
         // This is a 430
           pad.TextSize = 9;
      }
	    var font = new wgssSignatureSDK.Font(pad.Font, pad.TextSize);
      var varFont = new wgssSignatureSDK.Variant();
      varFont.Set(font);
	    wizCtlG.PutFont(varFont, onPutFont);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl GetPadWidth failed";
    }
  }
  
  function onGetPadHeight(wizCtlG, height, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl onGetPadHeight";
      pad.Height = height;
      document.getElementById("statusText").innerHTML += "<br>WizCtl onGetPadHeight: " + pad.Height;
      wizCtlG.GetPadWidth(onGetPadWidth);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl onGetPadHeight failed";
    }
  }
  
  function onPutFont(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PutFont";
      //wizCtlG.GetPadHeight(onGetPadHeight);
      // We have already got the height and width now so just continue with adding
      // the text objects to the wizctl
	  
      pad.x = new wgssSignatureSDK.Variant();
      pad.x.Set(pad.xText);
      pad.y = new wgssSignatureSDK.Variant();
      pad.y.Set(pad.yText);
      var txtPrompt =  new wgssSignatureSDK.Variant();
      txtPrompt.Set("Enter a 4 digit PIN code below");
      var option = new wgssSignatureSDK.Variant();
      wizCtlG.AddObject(wgssSignatureSDK.ObjectType.ObjectText, "txt", pad.x, pad.y, txtPrompt, 
                        option, onAddObjectText);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PutFont failed";
    }
  }
  
  function onReset(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Reset";
      document.getElementById("statusText").innerHTML += "<br>Setting font size to "+ pad.TextSize;
	  

      // Instead of setting the font first we want to get the pad dimensions so we know
      // what model the pad is and what size to use for the font
      wizCtlG.GetPadHeight(onGetPadHeight);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Reset failed";
    }
  }
  
  function onClose(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl window closed";
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Close failed";
    }
  }
  
  function onPadConnect(wizCtlG, padConnect, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status && 1 == padConnect)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PadConnect";
      wizCtlG.Reset(onReset);
    }
    else
    {
      wizCtlG.Close(onClose);
      document.getElementById("statusText").innerHTML += "<br>WizCtl PadConnect failed, closing window";
    }
  }

  function onPutVisibleWindow(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl window set visible";
      wizCtlG.PadConnect(onPadConnect);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PutVisibleWindow error";
    }
  }
  
  function onWizCtlPutLicence(wizCtlG, status) 
  {
    if (wgssSignatureSDK.ResponseStatus.OK == status) 
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl licence set successfully";
      wizCtlG.PutVisibleWindow(true, onPutVisibleWindow);
    }
    else 
    {
      document.getElementById("statusText").innerHTML += "WizCtl licensing error: " + status;
    }
  }
  
  function onCreateWizCtl(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl created";
      wizCtlG.PutLicence(LICENCEKEY, onWizCtlPutLicence);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Error creating WizCtl";
    }
  }
  
  function onSigCtlPutLicence(sigCtlV, status) 
  {
    if (wgssSignatureSDK.ResponseStatus.OK == status) 
    {
      document.getElementById("statusText").innerHTML += "<br>SigCtl licence set successfully";
      wizCtl = new wgssSignatureSDK.WizCtl(onCreateWizCtl);
    }
    else 
    {
      document.getElementById("statusText").innerHTML += "SigCtl licensing error: " + status;
    }
  }
  
  
  function onCreateSigCtl(sigCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>SigCtl created";
      sigCtlG.PutLicence(LICENCEKEY, onSigCtlPutLicence);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Error creating SigCtl";
      if(wgssSignatureSDK.ResponseStatus.INVALID_SESSION == status)
      {
        restartSession(wizPin);
      }
    }
  }
  
  // Class contains Pad Control data
  function CPadCtl_STU500() {
    Model = "STU-500";
    //Height = WizCtl.PadHeight;
    //Width  = WizCtl.PadWidth;
	//document.getElementById("statusText").innerHTML += "<br>Pad width + height: " + Width + " " + Height;
    Font = "Verdana";
    TextSize = 14;
    xText = 30;
    yText = 10;
    yLSText = 28;
    yButton = "bottom";
    xLeftButton = "left";
    xCentreButton = "centre";
    xRightButton = "right";
                  
    KeyWidth = 60;         // PIN pad
    PinSize = 20;
  }
  
  if (wgssSignatureSDK.running) 
  {
    if(null == canvas) 
    {
      canvas = document.getElementById("myCanvas");
      ctx = canvas.getContext("2d");
    }
    pad = new CPadCtl_STU500();
    sigCtl = new wgssSignatureSDK.SigCtl(onCreateSigCtl);
  }
}
// End of wizPin()

function wizCapture()
{
  var wizCtl;
  var sigCtl;
  var padHeight;
  var padWidth;
      
  function onRenderBitmap(sigObjV, bmpObj, status) 
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status) 
    {
      
      // console.log(bmpObj)
      if(bmpObj.isBase64)
      {
        document.getElementById("statusText").innerHTML += "<br>Base64 bitmap retrieved.";
				ctx.drawImage(bmpObj.image, 0, 0);
				updateDraft({image: bmpObj.image.src});
      }
      else
      {
        document.getElementById("statusText").innerHTML += "<br>Bitmap retrieved, rendering image...";
        
        var canvas = document.getElementById("myCanvas")
        var image = new Image();
        image.width = canvas.width;
        image.height = canvas.height;
        
        image.src = 'data:image/bmp;base64,' + bmpObj;
				ctx.drawImage(image, 0, 0);
				updateDraft({image: image.src});
      }
    } 
    else 
    {
      document.getElementById("statusText").innerHTML += "<br>Signature Render Bitmap error: " + status;
    }
  }
  
  function onGetSignature(sigCtlV, sigObjV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      // document.getElementById("statusText").innerHTML += "<br>Rendering Signature into bitmap";
      var flags = wgssSignatureSDK.RBFlags.RenderOutputBase64 |
                  wgssSignatureSDK.RBFlags.RenderColor24BPP;
      // sigObjV.RenderBitmap("png", canvas.width, canvas.height, 0.7, 0x00000000, 0x00FFFFFF, flags, 0, 0, onRenderBitmap);
      sigObjV.RenderBitmap("bmp", canvas.width, canvas.height, 0.7, 0x00000000, 0x00FFFFFF, flags, 0, 0, onRenderBitmap);

    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Error retrieving signature";
    }
  }
  
  function onDisplay(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>Displaying wizard";
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Error displaying";
    }
  }
  
  function onCloseWizard(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>Retrieving signature";
      sigCtl.GetSignature(onGetSignature);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Error closing wizard";
    }
  }
  
  function onPutZoom(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      function eventHandler(ctl, id, type, status)
      {
        if(wgssSignatureSDK.ResponseStatus.OK == status)
        {
          document.getElementById("statusText").innerHTML += "<br>EVENT " + id + " " + type;
          if("ok" ==  id)
          {
            document.getElementById("statusText").innerHTML += "<br>Closing wizard";
            wizCtlG.Close(onCloseWizard);
          }
          else if("cancel" == id)
          {
            document.getElementById("statusText").innerHTML += "<br>Closing wizard";
            wizCtlG.Close(function(wiz, status){});
          }
        }
        else
        {
          document.getElementById("statusText").innerHTML += "<br>EVENT WizCtl closed";
        }
      }
      document.getElementById("statusText").innerHTML += "<br>WizCtl Zoom 100.0";
      wizCtlG.SetEventHandler(eventHandler);
      setTimeout(wizCtlG.Display(onDisplay), 1000);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Zoom failed";
    }
  }
  
  function onAddOk(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status || true)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add Ok Button";
      wizCtlG.PutZoom(100, onPutZoom);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add Ok Button failed";
    }
  }
  
  function onAddSingature(wizCtlG, status){
    
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      var objData = new wgssSignatureSDK.Variant();
      
      addSignatureObject(objData, onAddOk)
    }
  }
  
  function onAddClear(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add Clear Button";
      var xVar = new wgssSignatureSDK.Variant();
      var yVar = new wgssSignatureSDK.Variant();
      var objData = new wgssSignatureSDK.Variant();
      var options = new wgssSignatureSDK.Variant();
      xVar.Set("right");
      yVar.Set("bottom");
      objData.Set("Ok");
      options.Set(110);
      wizCtlG.AddObject(
        wgssSignatureSDK.ObjectType.ObjectButton,
        "ok",
        xVar,
        yVar,
        objData,
        options,
        onAddSingature
      );
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add Clear Button failed";
    }
  }
  function onAddCancel(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add Cancel Button";
      var xVar = new wgssSignatureSDK.Variant();
      var yVar = new wgssSignatureSDK.Variant();
      var objData = new wgssSignatureSDK.Variant();
      var options = new wgssSignatureSDK.Variant();
      xVar.Set("center");
      yVar.Set("bottom");
      objData.Set("Clear");
      options.Set(110);
      wizCtlG.AddObject(
        wgssSignatureSDK.ObjectType.ObjectButton,
        "clear",
        xVar,
        yVar,
        objData,
        options,
        onAddClear
      );
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add Cancel Button failed";
    }
  }
  function onPutWho(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add who text";
      var xVar = new wgssSignatureSDK.Variant();
      var yVar = new wgssSignatureSDK.Variant();
      var objData = new wgssSignatureSDK.Variant();
      var options = new wgssSignatureSDK.Variant();
      xVar.Set("left");
      yVar.Set("bottom");
      objData.Set("Cancel");
      options.Set(110);
      wizCtlG.AddObject(
        wgssSignatureSDK.ObjectType.ObjectButton,
        "cancel",
        xVar,
        yVar,
        objData,
        options,
        onAddCancel
      );
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add who text failed";
    }
  }
  function onPutWhy(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add why text";
      var xVar = new wgssSignatureSDK.Variant();
      var yVar = new wgssSignatureSDK.Variant();
      var objData = new wgssSignatureSDK.Variant();
      var options = new wgssSignatureSDK.Variant();
      xVar.Set("left");
      yVar.Set(10);
      objData.Set("Contrato Antifraude Anual\n\n Gostaria de enfatizar que a execução dos pontos do programa cumpre um papel essencial na formulação de todos os recursos funcionais envolvidos. Não obstante, o novo modelo estrutural aqui preconizado causa impacto indireto na reavaliação do processo de comunicação como um todo. Assim mesmo, o comprometimento entre as equipes aponta para a melhoria das direções preferenciais no sentido do progresso. Todavia, a competitividade nas transações comerciais facilita a criação das posturas dos órgãos dirigentes com relação às suas atribuições. Do mesmo modo, a mobilidade dos capitais internacionais garante a contribuição de um grupo importante na determinação dos modos de operação convencionais.");
      
      wizCtlG.AddObject(
        wgssSignatureSDK.ObjectType.ObjectText,
        "who",
        xVar,
        yVar,
        objData,
        options,
        onPutWho
      );
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add why text failed";
    }
  }
  
  function onPutSignature(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add Signature Object";
      var xVar = new wgssSignatureSDK.Variant();
      var yVar = new wgssSignatureSDK.Variant();
      var objData = new wgssSignatureSDK.Variant();
      var options = new wgssSignatureSDK.Variant();
      xVar.Set("right");
      yVar.Set(400);
      objData.Set("Serasa");
      wizCtlG.AddObject(
        wgssSignatureSDK.ObjectType.ObjectText,
        "why",
        xVar,
        yVar,
        objData,
        options,
        onPutWhy
      );
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Add Signature Object failed";
    }
  }
  
  function onGetFont(wizCtlG, font, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl GetFont:";
      document.getElementById("statusText").innerHTML += "<br>Variant type " + font.type;
      document.getElementById("statusText").innerHTML += "<br>Font name " + font.fontName;
      document.getElementById("statusText").innerHTML += "<br>Font size " + font.fontSize;
      var xVar = new wgssSignatureSDK.Variant();
      var yVar = new wgssSignatureSDK.Variant();
      var objData = new wgssSignatureSDK.Variant();
      var options = new wgssSignatureSDK.Variant();
      objData.Set(sigCtl);
      wizCtlG.AddObject(
        wgssSignatureSDK.ObjectType.ObjectSignature,
        "sig",
        xVar,
        yVar,
        objData,
        options,
        onPutSignature
      );
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl GetFont failed";
    }
  }
  
  function onGetPadWidth(wizCtlG, width, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
	  var textSize;
	  
      padWidth = width;      
	  // Font sizes depend on the pad height and width
	  document.getElementById("statusText").innerHTML += "<br>Pad height and width: " + padHeight + " " + padWidth;
	  
      // Set up the font size depending on which pad is in use	 
	  if (padWidth == 396 && padHeight == 100)
	  {
	     textSize = 8;
	  }
	  else
	  if (padWidth == 320 && padHeight == 200)
	  {
	     // This is a 430
         textSize = 9;
	  }
	  else
	     textSize = 16;
		 
	  var font = new wgssSignatureSDK.Font("Verdana", textSize);
      var varFont = new wgssSignatureSDK.Variant();
      varFont.Set(font);
	  wizCtlG.PutFont(varFont, onPutFont);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl GetPadWidth failed";
    }
  }
  
  function onGetPadHeight(wizCtlG, height, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      padHeight = height;
	  document.getElementById("statusText").innerHTML += "<br>WizCtl onGetPadHeight: " + padHeight;
      wizCtlG.GetPadWidth(onGetPadWidth);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl onGetPadHeight failed";
    }
  }
  
  function onPutFont(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PutFont";
      wizCtlG.GetFont(onGetFont);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PutFont failed";
    }
  }
  
  function onReset(wizCtlG, status)
  {
    var fontSize; 
	
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Reset";
      wizCtlG.GetPadHeight(onGetPadHeight);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Reset failed";
    }
  }
  
  function onClose(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl window closed";
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl Close failed";
    }
  }

  function onPadConnect(wizCtlG, padConnect, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status && 1 == padConnect)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PadConnect";
      wizCtlG.Reset(onReset);
    }
    else
    {
      wizCtlG.Close(onClose);
      document.getElementById("statusText").innerHTML += "<br>WizCtl PadConnect failed, closing window";
    }
  }
  
  function onPutVisibleWindow(wizCtlG, status)
  {
    document.getElementById("statusText").innerHTML += "<br>Connecting to pad";
  
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl window set visible";
      wizCtlG.PadConnect(onPadConnect);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl PutVisibleWindow error";
    }
  }
  
  function onWizCtlPutLicence(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl licence created";
      wizCtlG.PutVisibleWindow(true, onPutVisibleWindow);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl licensing error";
    }
  }
  
  function onCreateWizCtl(wizCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>WizCtl created";
      wizCtlG.PutLicence(LICENCEKEY, onWizCtlPutLicence);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Error creating WizCtl";
    }
  }
  
  function onSigCtlPutLicence(sigCtlV, status) 
  {
    if (wgssSignatureSDK.ResponseStatus.OK == status) 
    {
      wizCtl = new wgssSignatureSDK.WizCtl(onCreateWizCtl);
    }
    else 
    {
      document.getElementById("statusText").innerHTML += "SigCtl licensing error: " + status;
    }
  }
  
  function onCreateSigCtl(sigCtlG, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>SigCtl created";
      sigCtlG.PutLicence(LICENCEKEY, onSigCtlPutLicence);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Error creating SigCtl";
      if(wgssSignatureSDK.ResponseStatus.INVALID_SESSION == status)
      {
        restartSession(wizCapture);
      }
    }
  }
  
  if (wgssSignatureSDK.running) 
  {
    if(null == canvas) 
    {
      canvas = document.getElementById("myCanvas");
      ctx = canvas.getContext("2d");
    }
    sigCtl = new wgssSignatureSDK.SigCtl(onCreateSigCtl);
  }else{
    alert("Driver/dispositivo não encontrado")
  }
}

function captureSignature() {
  var sigObj;
  var sigCtl;
  var dynCapt;
  
  function onPutSigData(sigObjV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>PutSigData called";
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>error on PutSigData: " + status;
    }
  }
  
  function onGetSigText(sigObjV, data, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      // document.getElementById("statusText").innerHTML += "<br>SigText: " + data;
      var vData = new wgssSignatureSDK.Variant();
      vData.type = wgssSignatureSDK.VariantType.VARIANT_BASE64;
      vData.base64 = data;
      sigObjV.PutSigData(vData, onPutSigData);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>error on GetSigData: " + status;
    }
  }
  
  function onGetAdditionalData(sigObjV, additionalData, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>Additional Data/MachineOS: " + additionalData;
      sigObjV.GetSigText(onGetSigText);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>error on GetAdditionalData: " + status;
    }
  }
  
  function onRenderBitmap(sigObjV, bmpObj, status) 
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status) 
    {
      if(bmpObj.isBase64)
      {
        document.getElementById("statusText").innerHTML += "<br>Base64 bitmap retrieved.<br>";
				// document.getElementById("statusText").innerHTML += "<br>" + bmpObj.image.src;
      }
      else
      {
        document.getElementById("statusText").innerHTML += "<br>Bitmap retrieved, rendering image...";
      }
			ctx.drawImage(bmpObj.image, 0, 0);
			updateDraft({image: bmpObj.image.src});
      sigObjV.GetAdditionalData(wgssSignatureSDK.CaptData.CaptMachineOS, onGetAdditionalData);
    } 
    else 
    {
      document.getElementById("statusText").innerHTML += "<br>Signature Render Bitmap error: " + status;
    }
  }
  
  function onPutExtraData(sigObjV, status) 
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status) 
    {
      document.getElementById("statusText").innerHTML += "<br>Rendering bitmap";
      var flags = wgssSignatureSDK.RBFlags.RenderOutputPicture |
                  wgssSignatureSDK.RBFlags.RenderColor24BPP;
      sigObjV.RenderBitmap("bmp", canvas.width, canvas.height, 0.7, 0x00000000, 0x00FFFFFF, flags, 0, 0, onRenderBitmap);
      sigObj = sigObjV;
    } 
    else 
    {
      document.getElementById("statusText").innerHTML += "<br>Signature PutExtraData error: " + status;
    }
  }
  
  function onDynCaptCapture(dynCaptV, sigObjV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      document.getElementById("statusText").innerHTML += "<br>Adding extra data";
      sigObjV.PutExtraData("extra key", "extra value", onPutExtraData);
    } 
    else if(1 == status) 
    {
      document.getElementById("statusText").innerHTML += "<br>Signature capture cancelled by the user";
    } 
    else 
    {
      document.getElementById("statusText").innerHTML += "<br>Signature capture error: " + status;
    }  
  }
        
  function onSigCtlPutLicence(sigCtlV, status)
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      dynCapt.Capture(sigCtlV, "name surnam e", "reason rr e a son", null, null, onDynCaptCapture);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>SigCtl constructor error: " + status;
    }
  }  

  function onSigCtlConstructor(sigCtlV, status)
  {
     if(wgssSignatureSDK.ResponseStatus.OK == status)
     {
        sigCtlV.PutLicence(LICENCEKEY, onSigCtlPutLicence);
     }
     else
     {
       document.getElementById("statusText").innerHTML += "<br>SigCtl constructor error: " + status;
     }
  }
  
  function onDynCaptConstructor(dynCaptV, status) 
  {
    if(wgssSignatureSDK.ResponseStatus.OK == status)
    {
      sigCtl = new wgssSignatureSDK.SigCtl(onSigCtlConstructor);
    }
    else
    {
      document.getElementById("statusText").innerHTML += "<br>Dynamic Capture constructor error: " + status;
      if(wgssSignatureSDK.ResponseStatus.INVALID_SESSION == status)
      {
        restartSession(captureSignature);
      }
    }
  }
            
  if (wgssSignatureSDK.running) 
  {
    if(null == canvas) 
    {
      canvas = document.getElementById("myCanvas");
      ctx = canvas.getContext("2d");
    }
    dynCapt = new wgssSignatureSDK.DynamicCapture(onDynCaptConstructor);
  }else{
    alert("Driver/dispositivo não encontrado")
  }
}
